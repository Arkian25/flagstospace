using System.Collections;
using UnityEngine;

public class FlagWindController : MonoBehaviour
{
    [Header("Wind Variables")]
    [SerializeField]
    private bool m_AlternateWindDirection;
    [SerializeField]
    private float m_MinWindSpeed;
    [SerializeField]
    private float m_MaxWindSpeed;

    private float m_WindDirection; // final rotation Y
    private float m_CurrentWindDirection; // current rotation Y
    private float m_CurrentWindSpeed;

    private void Awake()
    {
        m_CurrentWindDirection = m_AlternateWindDirection ? -180 : 0;
        m_WindDirection = m_AlternateWindDirection ? -180 : 0;
        var angles = transform.rotation.eulerAngles;
        angles.y = m_CurrentWindDirection;
    }


    public IEnumerator ChangeWindDirection(float windDirection)
    {
        bool add;
        if (m_WindDirection < windDirection)
            add = m_AlternateWindDirection ? false : true;
        else
            add = m_AlternateWindDirection ? true : false;      

        m_WindDirection = m_AlternateWindDirection? windDirection-180 : windDirection;
        if (m_WindDirection == -360)
            m_WindDirection = 0;
        m_CurrentWindSpeed = Random.Range(m_MinWindSpeed, m_MaxWindSpeed);
        Debug.Log(m_WindDirection);
        while (m_CurrentWindDirection != m_WindDirection)
        {
            if (add)
                m_CurrentWindDirection += 1f;
            else
                m_CurrentWindDirection -= 1f;
            var angles = transform.rotation.eulerAngles;
            angles.y = m_CurrentWindDirection;
            transform.rotation = Quaternion.Euler(angles);
            if (m_CurrentWindDirection == -360 || m_CurrentWindDirection == 360)
                m_CurrentWindDirection = 0;
            if (m_CurrentWindDirection == -180 || m_CurrentWindDirection == 180)
                m_CurrentWindDirection = -180;
            yield return new WaitForSeconds(1 / m_CurrentWindSpeed);
        }
    }

    public void StartWindDirectionCoroutine(float windDirection)
    {
        Debug.Log("Esta es la nueva direcci�n del viento: " + windDirection);
        StopAllCoroutines();
        StartCoroutine(ChangeWindDirection(windDirection));
    }
}
