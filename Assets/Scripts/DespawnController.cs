using UnityEngine;

public class DespawnController : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
            collision.gameObject.GetComponent<PlayerController>().Die();
        else
            Destroy(collision.gameObject);
    }
}
