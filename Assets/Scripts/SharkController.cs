using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharkController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DespawnCoroutine());
    }


    private IEnumerator DespawnCoroutine()
    {
        yield return new WaitForSeconds(5);
        Destroy(this.gameObject);
    }
}
