using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof( PointsController))]
public class PlayerController : MonoBehaviour
{
    private enum StateMachine { NONE, IDLE, MOVEMENT, AIR, DEAD }
    [SerializeField]
    private StateMachine m_CurrentState;

    private Vector3 m_Movement;

    [Header("Layers")]
    [SerializeField]
    private LayerMask m_GroundLayer;

    [Header("Inputs")]
    [SerializeField]
    private InputActionAsset m_InputActionAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;

    [Header("Stats")]
    [Header("JumpStats")]
    [SerializeField]
    private float m_JumpForce;
    [SerializeField]
    private float m_GravityOnFall;
    [SerializeField]
    private bool m_DoubleJumpAvailable;
    [SerializeField]
    private bool m_FirstJumpDone;
    [SerializeField]
    private bool m_JumpButtonPressed;
    [SerializeField]
    private float m_MaxJumpDurationTime;
    private float m_JumpCurrentDurationTime;
    [Header("MovementStats")]
    [SerializeField]
    private float m_MovementSpeed;
    [SerializeField]
    private float m_MaxMovementSpeed;
    private PointsController m_PointsController;
    //Components
    private Rigidbody2D m_Rigidbody;

    //Coroutines
    private Coroutine m_GravityCoroutine;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody2D>();
        m_PointsController = GetComponent<PointsController>();
        m_Input = Instantiate(m_InputActionAsset);
        m_MovementAction = m_Input.FindActionMap("PlayerMap").FindAction("Movement");
        m_Input.FindActionMap("PlayerMap").FindAction("Jump").performed += Jump;
        m_Input.FindActionMap("PlayerMap").FindAction("Jump").started += SwitchJumpButtonState;
        m_Input.FindActionMap("PlayerMap").FindAction("Jump").canceled += SwitchJumpButtonState;
        m_Input.FindActionMap("PlayerMap").Enable();
    }

    void Start()
    {
        m_DoubleJumpAvailable = true;
        m_JumpButtonPressed = false;
    }

    void Update()
    {
        UpdateState();
    }

    #region State Machine
    private void ChangeState(StateMachine state)
    {
        if (state == m_CurrentState)
            return;
        ExitState();
        InitState(state);
    }

    private void InitState(StateMachine newState)
    {
        m_CurrentState = newState;
        switch (m_CurrentState)
        {
            case StateMachine.IDLE:
                m_Rigidbody.velocity = new Vector3(0, m_Rigidbody.velocity.y, 0);
                break;
            case StateMachine.MOVEMENT:
                break;
            case StateMachine.AIR:
                m_GravityCoroutine = StartCoroutine(GravitySoftener());
                break;
            case StateMachine.DEAD:
                StopAllCoroutines();
                GameManager gameManager = GameManager.Instance;
                gameManager.ActivateMenuBox();
                Destroy(this.gameObject);
                break;
            default: break;
        }
    }

    private void UpdateState()
    {
        GroundCheker();
        switch (m_CurrentState)
        {
            case StateMachine.IDLE:
                if (m_MovementAction.ReadValue<Vector2>().x != 0)
                    ChangeState(StateMachine.MOVEMENT);
                break;
            case StateMachine.MOVEMENT:
                m_Movement = Vector3.zero;

                if (m_MovementAction.ReadValue<Vector2>().x > 0)
                    m_Movement += transform.right;
                else if (m_MovementAction.ReadValue<Vector2>().x < 0)
                    m_Movement -= transform.right;

                if (m_Movement != Vector3.zero)
                {
                    if (Mathf.Abs(m_Rigidbody.velocity.x) < m_MaxMovementSpeed)
                        m_Rigidbody.AddForce(m_Movement.normalized * m_MovementSpeed + Vector3.up * m_Rigidbody.velocity.y, ForceMode2D.Force);
                }
                else
                {
                    ChangeState(StateMachine.IDLE);
                }
                break;
            case StateMachine.AIR:
                m_Movement = Vector3.zero;

                if (m_MovementAction.ReadValue<Vector2>().x > 0)
                    m_Movement += transform.right;
                else if (m_MovementAction.ReadValue<Vector2>().x < 0)
                    m_Movement -= transform.right;

                if (m_Movement != Vector3.zero)
                {
                    Debug.Log("Legooooooo " + Mathf.Abs(m_Rigidbody.velocity.x));
                    if (Mathf.Abs(m_Rigidbody.velocity.x) < m_MaxMovementSpeed)
                        m_Rigidbody.AddForce(m_Movement.normalized * m_MovementSpeed, ForceMode2D.Force);
                }
                else
                {
                    m_Rigidbody.velocity = new Vector3(0, m_Rigidbody.velocity.y, 0);
                }
                break;
            case StateMachine.DEAD:
                break;
            default: break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case StateMachine.IDLE:
                break;
            case StateMachine.MOVEMENT:
                break;
            case StateMachine.AIR:
                ResetGravityScale();
                m_DoubleJumpAvailable = true;
                StopCoroutine(m_GravityCoroutine);
                break;
            case StateMachine.DEAD:
                break;
            default: break;
        }
    }
    #endregion

    private void GroundCheker()
    {
        RaycastHit2D hit2D = Physics2D.BoxCast(new Vector3(transform.position.x, transform.position.y - 1, transform.position.z), new Vector2(1.1f, 0.5f), 0, Vector2.zero, Mathf.Infinity, m_GroundLayer);
        if (hit2D.collider == null && m_CurrentState != StateMachine.AIR)
            ChangeState(StateMachine.AIR);
        else if (hit2D.collider != null && m_CurrentState == StateMachine.AIR)
        {
            if (m_MovementAction.ReadValue<Vector2>().x != 0)
                ChangeState(StateMachine.MOVEMENT);
            else
                ChangeState(StateMachine.IDLE);
            if (hit2D.collider.gameObject.GetComponent<SignalController>() != null)
            {
                hit2D.collider.gameObject.GetComponent<SignalController>().ActivateSignal();
            }
        }
    }

    private void Jump(InputAction.CallbackContext context)
    {
        if (m_CurrentState != StateMachine.DEAD)
        {
            m_PointsController.CheckYDifference();
            if (m_CurrentState != StateMachine.AIR)
            {
                ResetGravityScale();
                m_Rigidbody.AddForce(transform.up * m_JumpForce, ForceMode2D.Impulse);
                m_FirstJumpDone = true;
            }
            else if (m_DoubleJumpAvailable)
            {
                ResetGravityScale();
                m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, 0, 0);
                m_Rigidbody.AddForce(transform.up * m_JumpForce, ForceMode2D.Impulse);
                if (m_FirstJumpDone)
                {
                    m_DoubleJumpAvailable = false;
                    m_FirstJumpDone = false;
                }
                else
                    m_FirstJumpDone = true;
            }
        }
    }

    private IEnumerator GravitySoftener()
    {
        while (true)
        {
            if ((m_Rigidbody.velocity.y <= 0 || !m_JumpButtonPressed || m_JumpCurrentDurationTime >= m_MaxJumpDurationTime) && m_Rigidbody.gravityScale < m_GravityOnFall)
            {
                m_Rigidbody.gravityScale += 0.1f;
            }
            m_JumpCurrentDurationTime += Time.deltaTime;
            yield return null;
        }
    }

    private void ResetGravityScale()
    {
        m_Rigidbody.gravityScale = 1;
        m_JumpCurrentDurationTime = 0;
    }

    private void SwitchJumpButtonState(InputAction.CallbackContext context)
    {
        m_JumpButtonPressed = !m_JumpButtonPressed;
    }

    public void Die()
    {
        ChangeState(StateMachine.DEAD);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(new Vector3(transform.position.x, transform.position.y - 1, transform.position.z), new Vector3(1.1f, 0.5f, 0.5f));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemy"))
        {
            Die();
        }
    }
}
