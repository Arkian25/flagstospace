using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/StringUlong")]
public class GEStringUlong : GEGenerico<string, ulong> { }
