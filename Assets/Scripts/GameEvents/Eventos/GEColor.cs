using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Color")]
public class GEColor : GEGenerico<Color> { }
