using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameObjectInt")]
public class GEGameObjectInt : GEGenerico<GameObject, int> { }
