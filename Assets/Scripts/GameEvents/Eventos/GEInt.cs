using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/Int")]
public class GEInt : GEGenerico<int> { }
