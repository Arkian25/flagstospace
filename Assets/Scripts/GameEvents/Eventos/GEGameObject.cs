using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/GameObject")]
public class GEGameObject : GEGenerico<GameObject> { }
