using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "GameEvent/EnumInt")]
public class GEEnumInt : GEGenerico<System.Enum, int> { }
