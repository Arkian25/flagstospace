using System.Collections.Generic;
using UnityEngine;

public class GEGenerico<T> : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GEListenerGenerico<T>> eventListeners =
        new List<GEListenerGenerico<T>>();

    public void Raise(T parameter)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(parameter);
    }

    public void RegisterListener(GEListenerGenerico<T> listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GEListenerGenerico<T> listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }

}

public abstract class GEGenerico<T0, T1> : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GEListenerGenerico<T0, T1>> eventListeners =
        new List<GEListenerGenerico<T0, T1>>();

    public void Raise(T0 parameter0, T1 parameter1)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(parameter0, parameter1);
    }

    public void RegisterListener(GEListenerGenerico<T0, T1> listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GEListenerGenerico<T0, T1> listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}

public abstract class GEGenerico<T0, T1, T2> : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<GEListenerGenerico<T0, T1, T2>> eventListeners =
        new List<GEListenerGenerico<T0, T1, T2>>();

    public void Raise(T0 parameter0, T1 parameter1, T2 parameter2)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(parameter0, parameter1, parameter2);
    }

    public void RegisterListener(GEListenerGenerico<T0, T1, T2> listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GEListenerGenerico<T0, T1, T2> listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
