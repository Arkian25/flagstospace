using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GEListenerGenerico<T> : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GEGenerico<T> Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<T> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T parameter)
    {
        Response.Invoke(parameter);
    }
}
public abstract class GEListenerGenerico<T0, T1> : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GEGenerico<T0, T1> Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<T0, T1> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T0 parameter0, T1 parameter1)
    {
        Response.Invoke(parameter0, parameter1);
    }
}

public abstract class GEListenerGenerico<T0, T1, T2> : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GEGenerico<T0, T1, T2> Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<T0, T1, T2> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(T0 parameter0, T1 parameter1, T2 parameter2)
    {
        Response.Invoke(parameter0, parameter1, parameter2);
    }
}