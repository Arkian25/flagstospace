using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_HelpMenu;
    public void RetryPlayButton()
    {
        SceneManager.LoadScene("Game");
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    public void HomeButton()
    {
        SceneManager.LoadScene("Menu");
    }

    public void OpenHelpMenu()
    {
        if(m_HelpMenu)
            m_HelpMenu.SetActive(true);
    }
    public void CloseHelpMenu()
    {
        if (m_HelpMenu)
            m_HelpMenu.SetActive(false);
    }
}
