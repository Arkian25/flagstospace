using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SharkSignalController : SignalClass
{
    [SerializeField]
    private GameObject m_SharkPrefab;
    [SerializeField]
    private float m_SharkJumpForce;

    public override void DoReaction()
    {
        Debug.Log("Watch out! A shark is jumping!");    
        GameObject shark = Instantiate(m_SharkPrefab);
        shark.transform.position = transform.position - (Vector3.up * 10);
        shark.GetComponent<Rigidbody2D>().AddForce(Vector3.up * m_SharkJumpForce, ForceMode2D.Impulse);
    }
}
