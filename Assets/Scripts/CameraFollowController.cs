using UnityEngine;

public class CameraFollowController : MonoBehaviour
{
    [SerializeField]
    private float m_folowSpeed;
    [SerializeField]
    private Transform m_TargetTransform;
    private float m_PreviousTargetHeight;

    private void Start()
    {
        m_PreviousTargetHeight = m_TargetTransform.position.y;
        this.transform.position = new Vector3(transform.position.x, m_TargetTransform.position.y-1, -10f);
    }

    void Update()
    {
        if (m_TargetTransform != null && m_TargetTransform.position.y > m_PreviousTargetHeight)
        {
            GameManager gameManager = GameManager.Instance;
            gameManager.CheckPoleDistance();
            m_PreviousTargetHeight = m_TargetTransform.position.y;
            Vector3 newPos = new Vector3(transform.position.x, m_TargetTransform.position.y-1, -10f);
            this.transform.position = newPos;
        }
    }

}
