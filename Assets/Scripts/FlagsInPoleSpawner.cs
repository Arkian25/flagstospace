using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlagsInPoleSpawner : MonoBehaviour
{
    [Header("Lists")]
    [SerializeField]
    private List<Transform> m_ListOfSpawnPoints;
    [SerializeField]
    private List<GameObject> m_ListFlagsCanSpawn;

    
    void Start()
    {
        FlagSpawn();
    }

    
    void Update()
    {
        
    }

    private void FlagSpawn()
    {
        foreach (Transform t in m_ListOfSpawnPoints)
        {
            GameObject flag = Instantiate(m_ListFlagsCanSpawn[Random.Range(0, m_ListFlagsCanSpawn.Count)]);
            flag.transform.position = t.position;
        }
    }
}
