
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public List<GameObject> poleList;
    [SerializeField]
    private GameObject m_Player;
    [SerializeField]
    private GameObject m_PolePrefab;
    [SerializeField]
    private GameObject m_MenuBox;
    [SerializeField]
    private TextMeshProUGUI m_ScoreUGUI;
    private int points;
    private void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }

    }

    void Start()
    {
        points = 0;
        m_ScoreUGUI.text = points.ToString();
    }


    void Update()
    {

    }

    public void CheckPoleDistance()
    {
        float distance = (m_Player.transform.position - poleList[0].transform.position).y;
        if (distance >= 50)
            RemovePole();
    }

    private void RemovePole()
    {
        GameObject pole = poleList[0];
        poleList.RemoveAt(0);
        Destroy(pole);
        CreateNewPole();
    }

    private void CreateNewPole()
    {
        GameObject pole = Instantiate(m_PolePrefab);
        pole.transform.position = new Vector3(poleList[poleList.Count - 1].transform.position.x, poleList[poleList.Count - 1].transform.position.y + 45.11f, 0);
        poleList.Add(pole);
    }

    public void ActivateMenuBox()
    {
        m_MenuBox.SetActive(true);
    }

    public void AddPointsToScore()
    {
        if (points < 9999)
            points++;
        m_ScoreUGUI.text = points.ToString();
    }
}
