using System.Collections;
using UnityEngine;

public class ArrowWindController : MonoBehaviour
{
    private float m_WindDirection; // final rotation Y
    private float m_CurrentWindDirection; // current rotation Y

    [Header("Variables")]
    [SerializeField]
    private float m_MinTimeBetweenWindVariation;
    [SerializeField]
    private float m_MaxTimeBetweenWindVariation;
    [SerializeField]
    private float m_RotationVelocity;

    [Header("Events")]
    [SerializeField]
    private GEFloat m_WindChangeEvent;

    void Start()
    {
        StartCoroutine(WindDirectionChange());
    }

    private IEnumerator WindDirectionChange()
    {
        while (true)
        {
            int randomNumber = Random.Range(0, 2);

            m_CurrentWindDirection = m_WindDirection;
            if (randomNumber > 0)
                m_WindDirection = 0;
            else
                m_WindDirection = -180;

            bool add = true;
            if (m_CurrentWindDirection > m_WindDirection)
                add = false;

            while (m_CurrentWindDirection != m_WindDirection)
            {
                if (add)
                    m_CurrentWindDirection += 1f;
                else
                    m_CurrentWindDirection -= 1f;
                var angles = transform.rotation.eulerAngles;
                angles.z = m_CurrentWindDirection;
                transform.rotation = Quaternion.Euler(angles);
                if (m_CurrentWindDirection == -360 || m_CurrentWindDirection == 360)
                    m_CurrentWindDirection = 0;
                yield return new WaitForSeconds(1 / m_RotationVelocity);
            }
            Debug.Log("Lanzo evento " + m_WindDirection);
            m_WindChangeEvent.Raise(m_WindDirection);
            float waitTime = Random.Range(m_MinTimeBetweenWindVariation, m_MaxTimeBetweenWindVariation);
            yield return new WaitForSeconds(waitTime);
        }
    }
}
