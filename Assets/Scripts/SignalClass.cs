using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SignalClass : MonoBehaviour, IReactioner
{
    private void Start()
    {
        this.GetComponent<SpriteRenderer>().sprite = signalSprite;
    }
    public Sprite signalSprite;
    public abstract void DoReaction();
}
