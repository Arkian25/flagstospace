using UnityEngine;

public class PointsController : MonoBehaviour
{
    private float previousPosY;

    // Start is called before the first frame update
    void Start()
    {
        previousPosY = transform.position.y;
    }


    public void CheckYDifference()
    {
        if ((transform.position.y - previousPosY) > 3)
        {
            previousPosY = transform.position.y;
            GameManager gameManager = GameManager.Instance;
            gameManager.AddPointsToScore();
        }
    }
}
