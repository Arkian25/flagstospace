using UnityEngine;


public class SignalController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Reactioner;

    public void ActivateSignal()
    {
        if (m_Reactioner != null && m_Reactioner.GetComponent<SignalClass>() != null)
            m_Reactioner.GetComponent<SignalClass>().DoReaction();
    }

}
